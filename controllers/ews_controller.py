from flask import Blueprint, render_template, session, flash, redirect, request, jsonify
from utils import wrapper, secure
from exception.appexception import AppException
from exception.appexceptioncode import AppExceptionCode

ews_controller = Blueprint('ews_controller', __name__)


# IF_WP_001_CASE_CREATION
@ews_controller.route('/api/ews/v1/case-creation', methods=['POST'])
@secure
@wrapper
def case_creation():
    req = request.get_json()

    mandatory_keys = {'caseID', 'caseDate', 'caseTypeID', 'caseTypeName', 'caseTypeGroupCode', 'businessOwner', 'TIN',
                      'taxpayerName', 'objectLocation', 'assignment', 'wpData'}
    if not all(k in req for k in mandatory_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in root objects')

    assignment_data = req['assignment']
    assignment_keys = {'officeCode', 'officeName', 'assignmentDocNumber', 'assignmentDate', 'officers'}
    if not all(k in assignment_data for k in assignment_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in assignment objects')

    wp_data = req['wpData']
    if len(wp_data) == 0:
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in wpData')

    return 'Success'


# IF_WP_002_ASSIGNMENT_REVISION
@ews_controller.route('/api/ews/v1/assignment-revision', methods=['POST'])
@secure
@wrapper
def assignment_revision():
    req = request.get_json()

    mandatory_keys = {'caseID', 'caseDate', 'caseTypeID', 'caseTypeName', 'caseTypeGroupCode', 'businessOwner', 'TIN',
                      'taxpayerName', 'objectLocation', 'assignment'}
    if not all(k in req for k in mandatory_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD)

    assignment_data = req['assignment']
    assignment_keys = {'officeCode', 'officeName', 'assignmentDocNumber', 'assignmentDate', 'officers'}
    if not all(k in assignment_data for k in assignment_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in assignment objects')

    return 'Success'


# IF_WP_003_EWSTYPE_REVISION
@ews_controller.route('/api/ews/v1/ewstype-revision', methods=['POST'])
@secure
@wrapper
def ewstype_revision():
    req = request.get_json()

    mandatory_keys = {'caseID', 'caseDate', 'caseTypeID', 'caseTypeName', 'caseTypeGroupCode', 'businessOwner', 'TIN',
                      'taxpayerName', 'objectLocation', 'wpData'}
    if not all(k in req for k in mandatory_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD)

    wp_data = req['wpData']
    if len(wp_data) == 0:
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in wpData')

    return 'Success'


# IF_WP_004_CASE_APPROVAL
@ews_controller.route('/api/ews/v1/case-approval', methods=['POST'])
@secure
@wrapper
def case_approval():
    req = request.get_json()

    mandatory_keys = {'caseID', 'caseDate', 'caseTypeID', 'caseTypeName', 'caseTypeGroupCode', 'businessOwner', 'TIN',
                      'taxpayerName', 'objectLocation', 'caseApproval', 'outputDocuments'}
    if not all(k in req for k in mandatory_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD)

    case_approval_data = req['caseApproval']
    case_approval_keys = {'currentStep', 'approvalStatus', 'approvalDate', 'lastApprovalOfficerName',
                          'lastApprovalOfficerID'}

    if not all(k in case_approval_data for k in case_approval_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in caseApproval Object')

    output_docs = req['outputDocuments']
    if len(output_docs) == 0:
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in wpData')

    return 'Success'

# IF_WP_005_CASE_FINISHED
# @ews_controller.route('/api/ews/v1/case-finished', methods=['POST'])
# @secure
# @wrapper
# def case_finished():
#     req = request.get_json()

#     mandatory_keys = {'caseID', 'caseDate', 'caseTypeID', 'caseTypeName', 'caseTypeGroupCode', 'businessOwner', 'TIN',
#                      'taxpayerName', 'objectLocation', 'caseStatus', 'outputDocuments'}
#     if not all(k in req for k in mandatory_keys):
#         raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD)

#     output_docs = req['outputDocuments']
#     if len(output_docs) == 0:
#         raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'in wpData')

#     return 'Success'


