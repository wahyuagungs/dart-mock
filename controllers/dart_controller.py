from flask import Blueprint, render_template, session, flash, redirect, request, jsonify
from utils import wrapper, secure, get_random_text
from exception.appexception import AppException
from exception.appexceptioncode import AppExceptionCode
import base64
import os

dart_controller = Blueprint('dart_controller',__name__)


@dart_controller.route('/api/dart/v1/request', methods=['POST'])
@secure
@wrapper
def create_request():
    req = request.get_json()
    data = req['data']

    mandatory_keys = {'taxOfficeCode','requestId','requestName','requestPurpose','requestType','issuedBy','assignedOfficers'}
    if not all(k in data for k in mandatory_keys):
        raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD)

    # write files
    if 'documents' in data:
        for d in data['documents']:
            if d['fileContent'] is None or d['fileName'] is None:
                raise AppException(AppExceptionCode.INCOMPLETE_PAYLOAD, 'File is not found')
            write_file(d['fileContent'], d['fileName'])

    return 'Success'


def write_file(content, filename):
    try:
        if not os.path.isdir('data'):
            os.makedirs('data')

        file_path = os.path.join(os.getcwd(),'data', get_random_text(16) + '-' + filename)
        with open(file_path, "wb") as f:
            f.write(base64.decodebytes(bytes(content,'utf-8')))
    except Exception as ex:
        print(ex)
        raise AppException(AppExceptionCode.UNABLE_TO_WRITE_FILE)

