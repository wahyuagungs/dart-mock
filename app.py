from flask import Flask
import os

# create the Flask application
app = Flask(__name__)

app.config['RESTPLUS_MASK_SWAGGER'] = False
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = os.urandom(32)

from controllers.dart_controller import dart_controller
from controllers.ews_controller import ews_controller

app.register_blueprint(dart_controller)
app.register_blueprint(ews_controller)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
