from functools import wraps
from flask import jsonify, request
import uuid
from exception import AppExceptionCode
from exception.appexception import AppException


key = '1q2w3e4r5t6y7u8i9o0p'
api_version = 1


def error_output(message, code=0):
    return jsonify({"responseCode": code, "responseMessage": message})


def rest_output(data):
    return jsonify({"responseCode": 1, 'responseMessage': data})


def wrapper(function):
    @wraps(function)
    def decorated(*args, **kwargs):
        try:
            output = function(*args, **kwargs)
        except AppException as aex:
            return error_output(str(aex), aex.get_code())
        except Exception as ex:
            print(ex)
            return error_output('AppError-9999: ' + str(ex))
        return rest_output(output)
    return decorated


def secure(function):
    @wraps(function)
    def authenticated_resource(*args, **kwargs):
        if request.headers.get('token') == key and (request.headers.get('apiVersion') == '1' or
                                                    request.headers.get('apiVersion') == '1.0' or
                                                    request.headers.get('apiVersion') == '1,0'):
            return function(*args, **kwargs)
        else:
            return error_output('Incorrect Token or Headers', 1005)
    return authenticated_resource


def get_random_text(size=16):
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4())  # Convert UUID format to a Python string.
    random = random.replace("-", "")  # Remove the UUID '-'.
    return random[0:size]  # Return the random string.

